using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Repartir : MonoBehaviour
{

    int i = 0;
    int num;
    public Sprite[] CardSprite;
    int[] Values = new int[40];
    int currentIndex = 0;
    public GameObject Carta1;


    // Update is called once per frame
    void Update()
    {
        ValorCarta();

       
    }

    public void ValorCarta()
    {
        int num = 0;

        for (int i = 0; i < CardSprite.Length; i++)
        {
            num=i;
            num %= 10;

            if (num > 10 )
            {
                num = 10;
            }
            Values[i] = num++ + 1;

           
        }
     
        currentIndex = 1;
    }

    



    public void Barajar()
    {
        //Cuento hacia atras el lenght=40
        for (int i = CardSprite.Length - 1; i > 0; --i)
        {
        
            int r = Mathf.FloorToInt(Random.Range(0.0f, 1.0f) * CardSprite.Length - 1) + 1;
            Sprite vuelta = CardSprite[i];
            CardSprite[i] = CardSprite[r];
            CardSprite[r] = vuelta;

            int value = Values[i];
            Values[i] = Values[r];
            Values[r] = value;
        }
        currentIndex = 1;
    }
    

    public int DevolverValor(CardScript cardScript)
    {
        cardScript.PonerSprite(CardSprite[currentIndex]);
        cardScript.PonerValor(Values[currentIndex]);
        currentIndex++;
        return cardScript.ObtenerValorCarta();
        
    }

    public Sprite CartaDevuelta()
    {
        return CardSprite[0];
        
    }
    
}
