using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlMenu : MonoBehaviour
{

    public void Jugar()
    {
        SceneManager.LoadScene("Jugar");

    }

    public void Salir()
    {
        Debug.Log("Sali");
        Application.Quit();
    }
}
