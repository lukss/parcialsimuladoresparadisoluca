using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoverCarta : MonoBehaviour
{

    /* bool Moving = false;
     Vector3 LastPosition;


     [SerializeField] float angDrag = 0.05f;
     [SerializeField] float clamp = 0.5f;

     private void OnMouseButton()
     {
         LastPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition);
         Moving = true;
     }

     private void OnMouseUp()
     {
         Moving = false;
     }


     void update()
     {
         DragObject();
     }

     void DragObject()
     {
         if (Moving == true)
         {
             Vector3 RotationEffect = new Vector3(LastPosition.y - transform.position.y,  LastPosition.x - transform.position.x , LastPosition.x-transform.position.x) ;
             RotationEffect = Vector3.ClampMagnitude(RotationEffect * angDrag, clamp);
             transform.rotation = Quaternion.EulerAngles(RotationEffect);


             transform.position = Camera.main.ScreenToViewportPoint(Input.mousePosition) - (Vector3.forward * -10);
         }

         else if (Moving==false)
         {
             Quaternion value = Quaternion.Lerp(transform.rotation, Quaternion.identity, 0.1f);
             transform.rotation = value;


     }
    */

    private float Velocidad = 5f;
    private Vector3 PosicionObjetivo;
     void Start()
    {
        PosicionObjetivo = this.transform.position;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            PosicionObjetivo = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            PosicionObjetivo.z = this.transform.position.z;
        }

        this.transform.position = Vector3.MoveTowards(current: this.transform.position, target: PosicionObjetivo, Velocidad * Time.deltaTime); ;
        
    }



}