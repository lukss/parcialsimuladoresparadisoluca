using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    public Button btn_Repartir;
    public Button MoverC1;
    public Button MoverC2;
    public Button MoverC3;

    public GameObject Carta1;
    public GameObject Carta2;
    public GameObject Carta3;

    public MoverCarta Mov;
    public ControlPlayer1 controlPlayer;
    public ControlPlayer1 controlPlayer2;
    public ControlPlayer1 baraja;


    public Text txtScoreJ1;
    public int ScoreJ1;
    public Text ScoreC;
    public Text ScoreO;
    public Text Score7;
    public Text Score70;
    public Text ScoreE;
    public Text txtScoreJ2;
    public int ScoreJ2;
    private int marcador;

    private RaycastHit hit;
    [SerializeField] private Camera camera;
    public int mValue;
    public int valorB1;
    public int valorB2;
    public int valorB3;
    public int valorB4;
    public int valorCartaJ1;
    public int valorCartaJ2;
    public int conBaraja;
    public int puntajeJ1;
    public int puntajeJ2;
    public int puntaje;
    public int conBaraja2;
    public GameObject cartaJ1 = null;
    public GameObject cartaJ2 = null;
    public GameObject cartaB1;
    public GameObject cartaB2;
    public GameObject cartaB3;
    public GameObject cartaB4;
    public GameObject cartaSel;
    public GameObject cartaSel2;
    public GameObject cartaSel3;
    public GameObject cartaSel4;
    public GameObject cartaSel5;
    public GameObject cartaSel6;
    public GameObject btnTurno1;
    public GameObject btnTurno2;
    public float distanciaRayo;
    #region Bools
    public bool jugarJ1 = true;
    public bool jugarJ2 = false;
    bool estaPulsado;
    GameObject pieza;
#endregion


    // Start is called before the first frame update
    void Start()
    {
        btn_Repartir.onClick.AddListener(() => RepartirClicked());
        MoverC1.onClick.AddListener(() => MoverC1Clicked());
        MoverC2.onClick.AddListener(() => MoverC2Clicked());
        MoverC3.onClick.AddListener(() => MoverC3Clicked());

        marcador = 0;
        
    }


    public void ActualizarMarcador(int PuntosAsumar)
    {
        marcador += PuntosAsumar;
        ScoreE.text = "Puntos:" + marcador;
    }

    private void MoverC3Clicked()
    {
        GameObject.Find("Carta3").GetComponent<MoverCarta>();
        Mov.gameObject.SetActive(true);

    }

    private void MoverC2Clicked()
    {
        GameObject.Find("Carta2").GetComponent<MoverCarta>();
        Mov.gameObject.SetActive(true);
    }

    private void MoverC1Clicked()
    {
        GameObject.Find("Carta1").GetComponent<MoverCarta>();
        Mov.gameObject.SetActive(true);

    }

    public void RepartirClicked()
    {
        GameObject.Find("Repartir").GetComponent<Repartir>().Barajar();
        controlPlayer.StartMano();
        controlPlayer2.StartMano();
        baraja.StartBaraja();
        controlPlayer.ResetHand();
        controlPlayer2.ResetHand();

        //Score1.text=controlPlayer.ValorMano.ToString();
    }
  
    private void SetearTextos()
    {
        txtScoreJ1.text = "ScoreJ1: " + ScoreJ1.ToString();
    }


    private void SetearTextos2()
    {
        txtScoreJ2.text = "ScoreJ2:" + ScoreJ2.ToString();
    }


    // Update is called once per frame
    void Update()
    {
        SeleccionarCartaJ1();
        SeleccionarCartaJ2();
        // Score1.text = controlPlayer.valorMano.ToString();
        SetearTextos();
        SetearTextos2();
        if (Input.GetMouseButtonDown(1))
        {
           
   
        
            SeleccionarCarta();
            if (estaPulsado)
            {
                MoverPieza();
            }
        }
    
    }

    void SeleccionarCarta()
    {
        //Si estamos trabajando con Colliders 2D
        //RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        //if (hit)
        
        //Si estamos trabajando con Colliders 3D
        Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(rayo, out hit))
        {
            if (hit.collider.gameObject.tag == "Carta" || hit.collider.gameObject.tag == "Carta2" || hit.collider.gameObject.tag == "Carta3")
            {
                btnTurno1.gameObject.SetActive(true);
                estaPulsado = true;
                distanciaRayo = hit.distance; //distanciaRayo ser� un valor que utilizaremos m�s adelante
                pieza = hit.collider.gameObject;
             
            }
            else if(hit.collider.gameObject.tag == "Carta4" || hit.collider.gameObject.tag == "Carta5" || hit.collider.gameObject.tag == "Carta6")
            {
                btnTurno2.gameObject.SetActive(true);
                estaPulsado = true;
                distanciaRayo = hit.distance; //distanciaRayo ser� un valor que utilizaremos m�s adelante
                pieza = hit.collider.gameObject;
        
            }
        }
   

    }

    void MoverPieza()
    {
        Ray rayo = Camera.main.ScreenPointToRay(Input.mousePosition);
        Vector3 limiteRayo = rayo.GetPoint(distanciaRayo);
        limiteRayo = new Vector3(limiteRayo.x, limiteRayo.y, 0);
        pieza.transform.position = limiteRayo;
    }

    public void PasarTurnoJ1()
    {
        if (jugarJ1 == false || jugarJ1 == true)
        {
            cartaJ1 = null;
            valorCartaJ1 = 0;
            jugarJ1 = false;
            jugarJ2 = true;
            puntajeJ1 = 0;
            btnTurno1.gameObject.SetActive(false);

        }
    }
    public void PasarTurnoJ2()
    {
        if (jugarJ2 == false || jugarJ2 == true)
        {
            cartaJ2 = null;
            valorCartaJ2 = 0;
            jugarJ2 = false;
            jugarJ1 = true;
            puntajeJ1 = 0;
            btnTurno2.gameObject.SetActive(false);
        }
    }


    private void SeleccionarCartaJ1()
    {

        if (puntajeJ1 == 15 && jugarJ1 == true)
        {
            ScoreJ1++;
            SetearTextos();      
            jugarJ1 = false;
            Destroy(cartaJ1);
            Destroy(cartaB1);
            Destroy(cartaB2);
            Destroy(cartaB3);
            Destroy(cartaB4);
            Destroy(cartaSel);
            btnTurno1.gameObject.SetActive(true);
            //  jugarJ2 = true;
         /*   cartaJ1 = null;
            valorCartaJ1 = 0;
            jugarJ1 = false;
            jugarJ2 = true;
            puntajeJ1 = 0;*/
        }

        if (Input.GetMouseButtonDown(0)&& jugarJ1==true)
        {

            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.CompareTag("Carta")  && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ1 == null )
                {
                    //Debug.Log(hit.collider.name);
                    cartaJ1 = hit.collider.gameObject;
                    valorCartaJ1 = cartaJ1.GetComponent< CardScript> ().value;
                    cartaSel.gameObject.SetActive(true);
                  //  btnTurno1.gameObject.SetActive(true);
                    Debug.Log(mValue);
                    btnTurno1.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);

                }
                else if(hit.collider.CompareTag("Carta") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ1==hit.collider.gameObject&& cartaSel==true)
                {
                    cartaJ1 = null;
                    valorCartaJ1 =0;
                    cartaSel.gameObject.SetActive(false);
                    // btnTurno1.gameObject.SetActive(false);
                    btnTurno1.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                }

            
                if (hit.collider.CompareTag("Carta2") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ1 == null)
                {

                    //Debug.Log(hit.collider.name);
                    cartaJ1 = hit.collider.gameObject;
                    valorCartaJ1 = cartaJ1.GetComponent<CardScript>().value;
                    cartaSel2.gameObject.SetActive(true);
                    //  btnTurno1.gameObject.SetActive(true);
                    btnTurno1.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                    Debug.Log(mValue);

                }
                else if (hit.collider.CompareTag("Carta2") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ1 == hit.collider.gameObject && cartaSel2 == true)
                {
                    cartaJ1 = null;
                    valorCartaJ1 = 0;
                    cartaSel2.gameObject.SetActive(false);
                    btnTurno1.gameObject.SetActive(false);
                  
                }

                if (hit.collider.CompareTag("Carta3") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ1 == null)
                {
                    //Debug.Log(hit.collider.name);
                    cartaJ1 = hit.collider.gameObject;
                    valorCartaJ1 = cartaJ1.GetComponent<CardScript>().value;
                    cartaSel3.gameObject.SetActive(true);
                   // btnTurno1.gameObject.SetActive(true);
                    Debug.Log(mValue);
                    btnTurno1.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);

                }
                else if (hit.collider.CompareTag("Carta3") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ1 == hit.collider.gameObject && cartaSel3 == true)
                {
                    cartaJ1 = null;
                    valorCartaJ1 = 0;
                    cartaSel3.gameObject.SetActive(false);
                    //btnTurno1.gameObject.SetActive(false);

                }


                if (hit.collider.CompareTag("Baraja") && hit.collider.GetComponent<CardScript>().value != 0)
                {
                    //Debug.Log("1");
                    conBaraja++;
                    
                    if (conBaraja == 1)
                    {
                        valorB1 = hit.collider.GetComponent<CardScript>().value;

                        //Debug.Log("1");
                        cartaB1 = hit.collider.gameObject;
                        puntajeJ1 = valorCartaJ1 + valorB1;

                    }
                    if (conBaraja == 2)
                    {
                    
                        valorB2 = hit.collider.GetComponent<CardScript>().value;
                        cartaB2 = hit.collider.gameObject;
                        puntajeJ1 += valorB2;
             
                    }
                    if (conBaraja == 3)
                    {
                        valorB3 = hit.collider.GetComponent<CardScript>().value;
                        cartaB3 = hit.collider.gameObject;
                        puntajeJ1 += valorB3;
                  
                    }
                    if (conBaraja == 4)
                    {
                        valorB4 = hit.collider.GetComponent<CardScript>().value;
                        cartaB4 = hit.collider.gameObject;
                        puntajeJ1 += valorB4;
                        conBaraja = 0;
                   
                    

                    }
                }

             
                }


            }
        }
    private void SeleccionarCartaJ2()
    {
        if (puntajeJ2 == 15 && jugarJ2 == true)
        {
            ScoreJ2++;
            SetearTextos2();
            jugarJ2 = false;
            Destroy(cartaJ2);
            Destroy(cartaB1);
            Destroy(cartaB2);
            Destroy(cartaB3);
            Destroy(cartaB4);
            Destroy(cartaSel);
            // jugarJ1 = true;
            /* cartaJ2 = null;
             valorCartaJ2 = 0;
             jugarJ2 = false;
             jugarJ1 = true;
             puntajeJ1 = 0;*/
           btnTurno2.gameObject.SetActive(true);
        }

        if (Input.GetMouseButtonDown(0) && jugarJ2 == true)
        {
         
            
            Ray ray = camera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.CompareTag("Carta4") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ2 == null)
                {
                    //Debug.Log(hit.collider.name);
                    cartaJ2 = hit.collider.gameObject;
                    valorCartaJ2 = cartaJ2.GetComponent<CardScript>().value;
                    cartaSel4.gameObject.SetActive(true);
                    Debug.Log(mValue);
                    // btnTurno2.gameObject.SetActive(true);
                    btnTurno1.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                }
                else if (hit.collider.CompareTag("Carta4") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ2 == hit.collider.gameObject && cartaSel4 == true)
                {
                    cartaJ2 = null;
                    valorCartaJ2 = 0;
                    cartaSel4.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                }

                if (hit.collider.CompareTag("Carta5") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ2 == null)
                {
                    //Debug.Log(hit.collider.name);
                    cartaJ2 = hit.collider.gameObject;
                    valorCartaJ2 = cartaJ2.GetComponent<CardScript>().value;
                    cartaSel5.gameObject.SetActive(true);
                    Debug.Log(mValue);
                    //btnTurno2.gameObject.SetActive(true);
                    btnTurno1.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                }
                else if (hit.collider.CompareTag("Carta5") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ2 == hit.collider.gameObject && cartaSel5 == true)
                {
                    cartaJ2 = null;
                    valorCartaJ2 = 0;
                    cartaSel5.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                }
                if (hit.collider.CompareTag("Carta6") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ2 == null)
                {
                    //Debug.Log(hit.collider.name);
                    cartaJ2 = hit.collider.gameObject;
                    valorCartaJ2 = cartaJ2.GetComponent<CardScript>().value;
                    cartaSel6.gameObject.SetActive(true);
                    Debug.Log(mValue);
                    // btnTurno2.gameObject.SetActive(true);
                    btnTurno1.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                }
                else if (hit.collider.CompareTag("Carta6") && hit.collider.GetComponent<CardScript>().value != 0 && cartaJ2 == hit.collider.gameObject && cartaSel6 == true)
                {
                    cartaJ2 = null;
                    valorCartaJ2 = 0;
                    cartaSel6.gameObject.SetActive(false);
                    btnTurno2.gameObject.SetActive(false);
                }
                if (hit.collider.CompareTag("Baraja")&& hit.collider.GetComponent<CardScript>().value != 0)
                {
                    //Debug.Log("1");
                    conBaraja2++;

                    if (conBaraja2 == 1)
                    {
                        valorB1 = hit.collider.GetComponent<CardScript>().value;

                        //Debug.Log("1");
                        cartaB1 = hit.collider.gameObject;
                        puntajeJ2 = valorCartaJ2 + valorB1;

                    }
                    if (conBaraja2 == 2)
                    {

                        valorB2 = hit.collider.GetComponent<CardScript>().value;
                        cartaB2 = hit.collider.gameObject;
                        puntajeJ2 += valorB2;

                    }
                    if (conBaraja2 == 3)
                    {
                        valorB3 = hit.collider.GetComponent<CardScript>().value;
                        cartaB3 = hit.collider.gameObject;
                        puntajeJ2 += valorB3;

                    }
                    if (conBaraja2 == 4)
                    {
                        valorB4 = hit.collider.GetComponent<CardScript>().value;
                        cartaB4 = hit.collider.gameObject;
                        puntajeJ2 += valorB4;
                        conBaraja = 0;



                    }
                }


            }


        }
    }
    }
