using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPlayer1 : MonoBehaviour
{
    public CardScript cardScript;
    public Repartir repartir;

    public int valorMano = 0;
    


    private int Puntuacion=0;

    public GameObject[] mano;
    public int cardIndex = 0;
    public int OroCount = 0;
    public int SieteCount = 0;
    public int EscobaCount = 0;

    List<CardScript> oroList = new List<CardScript>();
    List<CardScript> SieteList = new List<CardScript>();
    List<CardScript> EscobaList = new List<CardScript>();
    

    public void StartMano()
    {
        ObtenerCarta();
        ObtenerCarta();
        ObtenerCarta();
    }

    public void StartBaraja()
    {
        ObtenerCarta();
        ObtenerCarta();
        ObtenerCarta();
        ObtenerCarta();
    }


  


    void Update()
    {
        
    }

    public int ObtenerCarta()
    {
        int cardValue = repartir.DevolverValor(mano[cardIndex].GetComponent<CardScript>());
        mano[cardIndex].GetComponent<Renderer>().enabled = true;
        valorMano += cardValue;

        if (cardValue == 1)
        {
            oroList.Add(mano[cardIndex].GetComponent<CardScript>());
            SieteList.Add(mano[cardIndex].GetComponent<CardScript>());
            EscobaList.Add(mano[cardIndex].GetComponent<CardScript>());
        }
        cardIndex++;
        return valorMano;
    }

    public void OroCheck()
    {
        foreach (CardScript oro in oroList)
        {
            
          
        }
    }

    public void ResetHand()
    {
        for (int i = 0; i < mano.Length; i++)
        {
            mano[i].GetComponent<CardScript>().ResetCarta();
            mano[i].GetComponent<Renderer>().enabled = false;
        }
        cardIndex = 0;
        valorMano = 0;

    }

}
