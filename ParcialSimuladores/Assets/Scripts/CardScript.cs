    using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CardScript : MonoBehaviour
{
    GameManager gameManager;
    public int value = 0;
   
 

    void SumarCarta()
    {
        
    }
    public int ObtenerValorCarta()
    {
        return value;
    }

    public void PonerValor(int newValue)
    {
        value = newValue;
    }



    public string ObtenerNombreSprite()
    {
        return GetComponent<SpriteRenderer>().sprite.name;
    }

    public void PonerSprite(Sprite newSprite)
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = newSprite;
    }

    public void ResetCarta()
    {
        Sprite back = GameObject.Find("RepartirController").GetComponent<Repartir>().CartaDevuelta();
        gameObject.GetComponent<SpriteRenderer>().sprite = back;
        value = 0;
    }

}
